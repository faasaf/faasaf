SHELL=/bin/bash
define color
\e[$(1)m$(2)\e[0m
endef
define logo 
\n \
\n \
$(call color,31,      .::                                         .::)\n \
$(call color,32,    .:                                          .:   )\n \
$(call color,33,  .:.: .:   .::       .::     .::::    .::    .:.: .:)\n \
$(call color,31,    .::   .::  .::  .::  .:: .::     .::  .::   .::  )\n \
$(call color,32,    .::  .::   .:: .::   .::   .::: .::   .::   .::  )\n \
$(call color,35,    .::  .::   .:: .::   .::     .::.::   .::   .::  )\n \
$(call color,36,    .::    .:: .:::  .:: .:::.:: .::  .:: .:::  .::  )\n \
\n
endef

VERSION ?= $(shell git branch --show-current | sed -r 's/\//-/g')
IMAGE_TAG_BASE ?= registry.gitlab.com/faasaf/faasaf

# Set the cluster provider (all|local|azure|linode)
providers=all local azure linode
PROVIDER=local
version=$(VERSION)-$(PROVIDER)
provider=$(PROVIDER)

ifeq ($(filter $(provider),$(providers)),)
provider=local
endif

# Image URL to use all building/pushing image targets
IMG ?= $(IMAGE_TAG_BASE):$(VERSION)-$(provider)

# Tags for playbooks
tags ?= all
# Options for playbooks
opts ?=
# Extra vars for playbooks
extra_vars ?=

# Path to kubeconfig
KUBE_DIR?=$(HOME)/.kube

# Mount kubeconfig
MNT_KUBE?=n
mount_kubeconfig_n=
mount_kubeconfig_y=-v $(KUBE_DIR):/etc/.kube
mount_kubeconfig_args=$(mount_kubeconfig_$(MNT_KUBE))

# Sets DEV mode
DEV?=n
DEV_n=
DEV_y=-v $(PWD):/opt/provision
dev_args=$(DEV_$(DEV))

define docker_run 
docker run --network host \
	--rm -ti \
	-v /var/run/docker.sock:/var/run/docker.sock \
	$(mount_kubeconfig_args) \
	$(dev_args) \
	$(IMG)
endef

define play
ANSIBLE_CONFIG="$(PWD)/ansible.cfg" \
	ansible-playbook \
	--tags "$(tags)" \
	-i hosts \
	--extra-vars "state=$(1) $(extra_vars) provider_context=$(PROVIDER)" \
	$(opts) \
	./playbooks/$*/index.yaml
endef

.PHONY: shell
shell: ## Open a shell in the faasaf environment.
	@echo $$'$(logo)'
	@$(docker_run) /bin/bash

.PHONY: help
help: ## Display this help.
	@echo $$'$(logo)'
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[^@:]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Build
.PHONY: docker-build
docker-build: ## Build docker image.
	@DOCKER_BUILDKIT=1 docker build --build-arg PROVIDER=$(provider) -t $(IMG) -f Dockerfile .

.PHONY: docker-push
docker-push: ## Push docker image
	@docker push $(IMG)

##@ Deployment

.PHONY: play
play: provider.play cluster.play faas.play ## Plays all components playbooks.

.PHONY: unplay
unplay: faas.unplay cluster.unplay provider.unplay ## Undos all components playbooks.

%.play: ## Play a playbook.
	@$(call play,present)

%.unplay: ## Unplay a playbok.
	@$(call play,absent)
