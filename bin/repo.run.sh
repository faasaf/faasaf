#/bin/env bash
cp -r /opt/repository/hosts /opt/provision
set -a
source /opt/repository/.env
set +a
exec "$@"
