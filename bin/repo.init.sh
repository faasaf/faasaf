#!/usr/bin/env bash

FAASAF_VERSION=blackchicken
cp -vr /opt/provision/makefiles /opt/repository/.

cat << 'EOF' > /opt/repository/Makefile
include ./vars.mk
include ./makefiles/repository.mk
EOF

cat << 'EOF' > /opt/repository/vars.mk
FAASAF_VERSION=blackchicken
PLAYBOOKS=$(wilcard playbooks/*)
PACKAGES=$(wildcard workspace/apps/*)
EOF

touch /opt/repository/.env

cd /opt/repository
make help
