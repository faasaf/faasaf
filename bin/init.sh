#!/usr/bin/env bash

docker run \
	--entrypoint=/bin/bash \
	--rm \
	-t \
	-e TERM=xterm \
	-u ${UID}:${UID} \
	-w /opt/provision \
	-v ${PWD}:/opt/repository \
	registry.gitlab.com/faasaf/faasaf:blackchicken-local \
	-c "/opt/provision/bin/repo.init.sh"
