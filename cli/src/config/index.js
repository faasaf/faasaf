import configYaml from "config-yaml";
import { createHash } from "crypto";
import fs from "fs";
import lodash from "lodash";
import { parse, stringify } from "yaml";
import { packagePath, repositoryPath, writeToRepository } from "../utils/fs.js";
import { logger } from "../logger/index.js";

const expressionTest = /("?\(\) => .*"?)|(\$\{[^}]+\})/;

const getConfigPath = (provider, basePath) => {
  const pathToCheck = `${basePath}/config/${provider}.yaml`;

  if (fs.existsSync(pathToCheck)) {
    return pathToCheck;
  }

  return `${basePath}/faasaf.config.yaml`;
};

const mergeConfig = (path, defaultConfig) => {
  if (fs.existsSync(path)) {
    const userConfig = configYaml(path);
    return lodash.merge(defaultConfig, userConfig);
  }
  return defaultConfig;
};

const recurseEphemeralConfig = (ephemeralConfig, parent) => {
  if (!ephemeralConfig) {
    return;
  }
  Object.entries(ephemeralConfig).forEach(([key, entry]) => {
    if (typeof entry === "object") {
      ephemeralConfig[key] = recurseEphemeralConfig(entry, parent);
      return;
    }

    const entryMatches = expressionTest.exec(entry);
    if (!entryMatches) {
      return;
    }

    const path = entryMatches[1] ?? entryMatches[2];
    const replaceValue = getReplaceValueFor(path, parent);
    ephemeralConfig[key] = isNonscalar(replaceValue)
      ? recurseEphemeralConfig(replaceValue, parent)
      : entry.replace(path, replaceValue);
  });

  return ephemeralConfig;
};

export const expandValues = (config) => {
  let configString = stringify(config);
  const maxIterations = Math.pow(Object.values(config).length, 2);
  let iteration = 0;

  while (expressionTest.test(configString) && maxIterations > iteration) {
    ++iteration;
    const parsedConfig = parse(configString);
    const ephemeralConfig = recurseEphemeralConfig(parsedConfig, parsedConfig);
    configString = stringify(ephemeralConfig);
  }

  return parse(configString);
};

const isNonscalar = (value) => {
  const scalarTypes = ["string", "number", "boolean", "symbol"];
  return !scalarTypes.includes(typeof value);
};

export const contextFunctions = {
  hash: (type, input) => createHash(type).update(input).digest("hex"),
  base64: (input, decode = false) =>
    decode
      ? Buffer.from(input, "base64").toString("utf8")
      : Buffer.from(input, "utf8").toString("base64"),
};

const evalConfigFunction = (configFunction, document) => {
  return function () {
    return eval(configFunction);
  }.call({
    config: document,
    ...contextFunctions,
  });
};

export const getReplaceValueFor = (path, document) => {
  const functionRegExp = /^"?(\(\) => .*)"?$/;
  const functionMatches = functionRegExp.exec(path);
  if (functionMatches) {
    const result = evalConfigFunction(functionMatches[1], document)();
    return result;
  }
  const propPath = path.replace("${", "").replace("}", "");
  const value = lodash.get(document, propPath);
  return value;
};

export const writeAnsibleVariables = (config) =>
  writeToRepository(".variables.json", JSON.stringify(config)).catch((e) => {
    logger.error("Error while writing ansible variables.", e);
  });

export const loadConfig = (provider, base, useDefaults = false) => {
  const defaultConfigPath = useDefaults && getConfigPath(provider, packagePath);
  const defaultConfig = useDefaults ? configYaml(defaultConfigPath) : {};

  const userConfigPath = getConfigPath(provider, repositoryPath);
  const mergedConfig = mergeConfig(userConfigPath, {
    ...defaultConfig,
    env: process.env,
    ...base,
  });

  return expandValues(mergedConfig);
};
