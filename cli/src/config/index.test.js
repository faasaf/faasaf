import { contextFunctions, expandValues, getReplaceValueFor } from "./index.js";
import should from "should";
import {describe, it} from "mocha"

describe("config", () => {
  describe("getReplaceValueFor", () => {
    it("executes function", () => {
      const context = {
        hello: "world",
      };

      const input = "() => `hello ${this.config.hello}`";
      const output = getReplaceValueFor(input, context);

      should(output).be.equal("hello world");
    });

    it("gets value from context", () => {
      const context = {
        hello: "world",
      };

      const input = "hello";
      const output = getReplaceValueFor(input, context);

      should(output).be.equal("world");
    });
  });

  describe("expandValues", () => {
    it("expands document values", () => {
      const document = {
        complexNo: "() => this.config.say == 'hello world' ? 'yes' : 'no'",
        quadroupleSay: "${doubleSay} ${doubleSay}",
        say: "${hello} world",
        doubleSay: "${say} ${say}",
        hello: "hello",
        complexYes: "() => this.config.say == 'hello world' ? 'yes' : 'no'",
        hash: "() => this.hash('sha256', this.config.say).substring(0, 6)",
        deepComplex: "() => `${this.config.hello} ${this.config.hello == 'hello' ? 'world': ''}`",
      };

      const output = expandValues(document);
      should(output.say).be.equal("hello world");
      should(output.complexNo).be.equal("no");
      should(output.complexYes).be.equal("yes");
      should(output.doubleSay).be.equal("hello world hello world");
      should(output.quadroupleSay).be.equal(
        "hello world hello world hello world hello world"
      );
      should(output.hash).be.equal(
        contextFunctions.hash("sha256", "hello world").substring(0, 6)
      );
      should(output.deepComplex).be.equal("hello world");
    });
  });
});
