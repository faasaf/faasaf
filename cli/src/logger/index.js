import { debugHandler, errorHandler, infoHandler, spinnerHandler, spinnerStartHandler, spinnerStopHandler, tableHandler } from "./stdout.js";
import * as dotenv from "dotenv";

dotenv.config();

const createLogger = (debug) => ({
  handlers: {},
  setHandler(type, key, handler) {
    if (!this.handlers[type]) {
      this.handlers[type] = {};
    }

    this.handlers[type][key] = handler;
  },
  log(type, ...args) {
    if (!this.handlers[type]) {
      return;
    }

    const meLogger = this;

    Promise.all(Object.values(this.handlers[type]).map((h) => h(meLogger, ...args)));
  },
  info(...args) {
    this.log("info", ...args);
  },
  debug(...args) {
    if (debug) {
      this.log("debug", ...args);
    }
  },
  error(...args) {
    this.log("error", ...args);
  }
});
export const logger = createLogger(!!process.env.FAASAF_CLI_DEBUG);

logger.setHandler("debug", "stdout", debugHandler);
logger.setHandler("table", "stdout", tableHandler);
logger.setHandler("error", "stdout", errorHandler);
logger.setHandler("info", "stdout", infoHandler);
logger.setHandler("spinner:start", "stdout", spinnerStartHandler);
logger.setHandler("spinner", "stdout", spinnerHandler);
logger.setHandler("spinner:stop", "stdout", spinnerStopHandler);
