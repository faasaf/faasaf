import { ChalkLogger } from "@guanghechen/chalk-logger";
import chalkTable from "chalk-table";
import ora from "ora";

const chalkLogger = new ChalkLogger({
  name: "faasaf-cli",
  level: "debug",
});

const spinner = ora("");

export const debugHandler = async (logger, ...args) => {
  chalkLogger.debug(JSON.stringify(args, undefined, 2));
};

export const infoHandler = async (logger, message) => {
  chalkLogger.info(message);
}

export const tableHandler = async (logger, table) => {
  const ctable = chalkTable(table.options, table.data);
  console.log("")
  console.log("")
  console.log(table.title);
  console.log("")
  console.log(ctable);
};

export const errorHandler = async (logger, message, data) => {
  chalkLogger.error(message);
  logger.debug(data);
}

export const spinnerStartHandler = (logger, preText, text) => {
  logger.log("spinner", preText, text);
  spinner.start();
}

export const spinnerHandler = (logger, preText, text) => {
  spinner.text = text;
  spinner.prefixText = preText;
}

export const spinnerStopHandler = (logger, preText, text)  => {
  logger.log("spinner", preText, text);
  spinner.stop();
}