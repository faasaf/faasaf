import util from "util";
import { globals } from "../../command.js";

const commandArgs = [];

const options = [
  {
    flags: "-r --raw",
    help: "Raw json output.",
  },
];

const action = (options) => {
  const { config } = globals;
  if (options.raw) {
    return console.log(JSON.stringify(config));
  }

  console.log(
    util.inspect(config, { showHidden: false, depth: null, colors: true })
  );
};

export const command = {
  name: "config",
  description: "Print computed configuration.",
  action,
  arguments: commandArgs,
  options,
};
