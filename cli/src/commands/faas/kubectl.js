import os from "os";
import k8s from "@kubernetes/client-node";
import { globals } from "../../command.js";

export const kubectl = () => {
  const { config } = globals;
  const kc = new k8s.KubeConfig();
  const context = `${config.ansible.cluster_name}-${config.ansible.provider.locations[0]}`;
  kc.loadFromFile(`${os.homedir()}/.kube/${context}`);
  kc.setCurrentContext(context);
  return kc.makeApiClient(k8s.CoreV1Api);
};
