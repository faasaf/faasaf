import shelljs from "shelljs";
import {
  readFromRepository,
  repositoryPath,
  writeToRepository,
} from "../../utils/fs.js";
import { parse, stringify } from "yaml";

const newFunction = async (functionName, { baseDir, lang }) => {
  shelljs.mkdir("-p", `${repositoryPath}/${baseDir}`);
  shelljs
    .cd(`${repositoryPath}/${baseDir}`)
    .exec(`faas-cli new ${functionName} --lang ${lang}`);
  const functionConfig = parse(
    await readFromRepository(`${baseDir}/${functionName}.yml`).then((b) =>
      b.toString()
    )
  );
  functionConfig.provider.gateway = "${ansible.faas.openfaas.ingress_host}";
  functionConfig.functions[
    functionName
  ].image = `\${repository.registry}/${functionName}:\${repository.version}`;

  writeToRepository(
    `${baseDir}/${functionName}.yml`,
    stringify(functionConfig)
  );
};

export default {
  name: "new",
  description: "create new OpenFaaS function.",
  action: newFunction,
  arguments: [
    {
      name: "functionName",
      help: "name of function to create.",
    },
  ],
  options: [
    {
      flags: "--lang <lang>",
      help: "language template to use.",
    },
    {
      flags: "--base-dir <baseDir>",
      help: "functions base directory",
      default: "workspace/functions",
    },
  ],
};
