import { globals } from "../../command.js";
import { kubectl } from "./kubectl.js";
import shelljs from 'shelljs';

const login = async () => {
  const {body:{data: basicAuth}} = await kubectl(globals.config).readNamespacedSecret(
    "basic-auth",
    "faas"
  );

  const faasGateway = config.ansible.faas.openfaas.ingress_host
  shelljs.exec(`faas-cli login -u ${atob(basicAuth['basic-auth-user'])} admin -p ${atob(basicAuth['basic-auth-password'])} -g ${faasGateway} --tls-no-verify  --timeout 30s`)
};

export default {
  name: "login",
  description: "login into OpenFaaS provider.",
  action: login,
  arguments: [],
  options: [],
};
