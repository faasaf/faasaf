import crypto from "crypto";
import fs from "fs";
import { globby } from "globby";
import shelljs from "shelljs";
import { parse, stringify } from "yaml";
import { globals } from "../../command.js";
import { expandTemplate } from "../../templates/template.js";
import {
  deleteFromRepository,
  repositoryPath,
  writeToRepository,
} from "../../utils/fs.js";

const up = async (functionName, { baseDir }) => {
  const { config } = globals;
  const functionConfigs = await lsFunctions(functionName, baseDir);
  let provider = {};
  const stack = {
    version: "1.0",
    provider: {},
    functions: functionConfigs
      .map((funcConf) => {
        const parsed = parse(
          expandTemplate(fs.readFileSync(funcConf).toString(), config)
        );
        provider = parsed.provider;
        return parsed;
      })
      .reduce(
        (carry, fc) => ({
          ...carry,
          ...fc.functions,
        }),
        {}
      ),
  };
  stack.provider = provider;
  const rawStack = stringify(stack);
  const md5 = crypto.createHash("md5").update(rawStack).digest("hex");
  await writeToRepository(`${baseDir}/.${md5}.yml`, rawStack);

  shelljs
    .cd(`${repositoryPath}/${baseDir}`)
    .exec(
      `faas-cli up -f .${md5}.yml --tls-no-verify -n ${config.ansible.function_namespace}`
    );

  await deleteFromRepository(`${baseDir}/.${md5}.yml`);
};

export default {
  name: "up",
  description: "build and deploy a OpenFaaS function.",
  action: up,
  arguments: [
    {
      name: "functionName",
      help: "name of function to create.",
      default: "*",
    },
  ],
  options: [
    {
      flags: "--base-dir <baseDir>",
      help: "functions base directory",
      default: "workspace/functions",
    },
  ],
};

const lsFunctions = async (functionName, baseDir) =>
  await globby(`${repositoryPath}/${baseDir}/${functionName}.yml`);
