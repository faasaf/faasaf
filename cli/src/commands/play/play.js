import { repositoryPath } from "../../utils/fs.js";
import { globby } from "globby";
import docker from "./docker.js";
import { logger } from "../../logger/index.js";
import { program } from "commander";
import path from "path";

export const runPlaybooks = async (config, reel, ansibleContext) => {
  let toPlay = reel;
  if (config.state === "absent") {
    toPlay = reel.reverse;
  }

  for (let index in toPlay) {
    ansibleContext.playbook = toPlay[index];
    const response = await runPlaybook(config, ansibleContext);
    if (response.code > 0) {
      program.error(`Error occured during ${ansibleContext.playbook}.`);
    }
  }
};

export const runPlaybook = async (config, ansibleContext) => {
  const playbooksDir = `playbooks`;

  const cmd = ansiblePlaybook(
    `/opt/provision/${playbooksDir}/${ansibleContext.playbook}`,
    ansibleContext
  );

  const version = (() => {
    if (
      (!config.parent_group || config.parent_group === "local") &&
      !["azure", "linode"].includes(config.provider_context)
    ) {
      return `${config.version}-local`;
    }

    return `${config.version}-${config.provider_context}`;
  })();

  const image = config.faasaf_image ?? "registry.gitlab.com/faasaf/faasaf";

  return runInDocker(cmd, {
    ...config,
    ansible: ansibleContext.variables,
    version,
    image,
    kubeconfig: ansibleContext.kubeconfig,
  });
};

const ansiblePlaybook = (playbook, { tags, skipTags, verbose }) => `
ANSIBLE_CONFIG="\${PWD}/ansible.cfg" \
	ansible-playbook \
	--tags "${tags}" \
  ${skipTags ? `--skip-tags ${skipTags} \\` : "\\"}
	-i hosts \
  ${(verbose && "-vvvvv \\") || "\\"}
	--extra-vars "@/opt/repository/.variables.json" \
	${playbook}/index.yaml
`;

const runInDocker = async (cmd, { version, image, kubeconfig, playbooks }) => {
  logger.debug(`Running command ${cmd}`);
  const volumeBinds = await listPlaybookDirectories(playbooks)
    .then((d) =>
      d.map(
        (directory) =>
          `${directory}:/opt/provision/playbooks/${path.basename(directory)}`
      )
    )
    .then((vb) =>
      vb.concat(
        `${repositoryPath}:/opt/repository`,
        `${repositoryPath}/hosts:/opt/provision/hosts`,
        `${kubeconfig}:/etc/.kube`
      )
    );

  const faasafDockerImage = `${image}:${version}`;
  if (!process.env.FAASAF_CLI_DEBUG) {
    docker.pull(faasafDockerImage);
  }

  const response = await docker
    .run(faasafDockerImage, cmd, volumeBinds, process.stdout)
    .then((o) => ({
      code: o.StatusCode,
    }));

  return response;
};

const listPlaybookDirectories = (playbooks) => {
  return globby(`${repositoryPath}/${playbooks}`, {
    onlyFiles: false,
    deep: 0,
  });
};
