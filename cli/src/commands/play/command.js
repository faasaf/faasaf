import os from "os";
import path from "path";
import { writeAnsibleVariables } from "../../config/index.js";
import { repositoryPath } from "../../utils/fs.js";
import { runPlaybook, runPlaybooks } from "./play.js";
import { globals } from "../../command.js";

const commandArgs = [
  {
    name: "playbook",
    help: "Runs ansible playbook.",
    default: "*",
  },
];

const options = [
  {
    flags: "-t --tags <tags>",
    help: "comma separated tags to apply in playbook runner.",
    default: "all",
  },
  {
    flags: "-s --state <state>",
    help: "set state. (present|absent)",
    default: "present",
  },
  {
    flags: "--skip-tags <skipTags>",
    help: "comma separated tags to skip in playbook runner.",
  },
  {
    flags: "--kubeconfig <kubeconfig>",
    help: "path to your local kubeconfig folder.",
    default: "~/.kube",
  },
  {
    flags: "-v --verbose",
    help: "verbose output.",
  },
];

const action = async (playbook, options) => {
  const { config } = globals;
  const kubeconfig =
    options.kubeconfig[0] === "~"
      ? path.join(os.homedir(), options.kubeconfig.substr(1))
      : options.kubeconfig;

  const ansibleContext = {
    playbook,
    kubeconfig,
    tags: options.tags === "all" ? "all" : `${options.tags}`,
    skipTags: options.skipTags ? `[${options.skipTags}]` : null,
    hosts: `${repositoryPath}/hosts`,
    verbose: !!options.verbose,
    variables: {
      ...config.ansible,
      state: options.state ?? "present",
      hostpath: repositoryPath,
      provider_context: config.provider_context.replace("-", "_"),
    },
  };

  await writeAnsibleVariables(ansibleContext.variables);
  if (playbook === "*") {
    const playbooks = config.ansible.play;
    return await runPlaybooks(config, playbooks, ansibleContext);
  }

  return await runPlaybook(config, ansibleContext);
};

export const command = {
  name: "play",
  description: "Runs an ansible playbook.",
  arguments: commandArgs,
  options,
  action,
};
