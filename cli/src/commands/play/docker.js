import Docker from "dockerode";
import os from "os";
import shelljs from "shelljs";

const docker = new Docker();

export default {
  pull: (image) => {
    shelljs.exec(`docker pull ${image}`);
  },
  run: (image, cmd, binds, output) => {
    const Volumes = binds.reduce(
      (carry, b) => ({
        ...carry,
        [`${b.split(":")[1]}`]: {},
      }),
      {}
    );
    if (process.platform !== "darwin" && os.userInfo().uid !== 0) {
      const userMap = `adduser -D faasaf -u ${os.userInfo().uid} \
      && mkdir -p /opt/provision/.ansible \
      && chown -R faasaf:faasaf /etc/.kube /opt/provision \
      && su -s /bin/bash faasaf -c`;

      cmd = `${userMap} "${cmd}"`;
    }

    return docker
      .run(image, cmd, output, {
        Volumes,
        Hostconfig: {
          Binds: binds,
        },
        Env: Object.entries(process.env).map(([k, v]) => `${k}=${v}`),
        WorkingDir: "/opt/provision",
        Entrypoint: ["/bin/bash", "-c"],
      })
      .then(([output, container]) => {
        container.remove();
        return output;
      });
  },
};
