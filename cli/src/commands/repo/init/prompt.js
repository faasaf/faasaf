import { basename } from "path";
import prompts from "prompts";
import { repositoryPath } from "../../../utils/fs.js";

export const promptDefaultConfig = async () => {
  return prompts([
    {
      type: "text",
      name: "repositoryName",
      message: "What is the name of your repository?",
      initial: `${basename(repositoryPath)}`,
    },
    {
      type: "confirm",
      name: "clusterNameSame",
      message: "Would you like to use the same as the cluster name?",
      initial: true,
    },
    {
      type: (prev) => !prev && "text",
      name: "clusterName",
      message: "Please provide a cluster name.",
    },
  ]);
};
