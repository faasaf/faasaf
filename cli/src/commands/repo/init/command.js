import fs from "fs";
import lodash from "lodash";
import shell from "shelljs";
import { parse, stringify } from "yaml";
import {
  packagePath,
  readFromPackage,
  repositoryPath,
  writeToRepository,
} from "../../../utils/fs.js";
import { promptInventory } from "../inventory/prompts.js";
import { promptDefaultConfig } from "./prompt.js";

const commandArgs = [];
const options = [];

const action = async () => {
  const ansibleInventory = {
    all: {
      hosts: {},
      children: {},
    },
  };

  const defaultConfigPrompt = await promptDefaultConfig();
  const inventoryPrompt = await promptInventory(ansibleInventory);

  const runConfig = await readFromPackage(".faasafrc").then((rc) =>
    parse(rc.toString())
  );

  const userConfig = {
    repository: {
      name: defaultConfigPrompt.repositoryName,
    },
    ansible: {
      cluster_name: defaultConfigPrompt.clusterNameSame
        ? defaultConfigPrompt.repositoryName
        : defaultConfigPrompt.clusterName,
    },
  };

  const rawConfig = await readFromPackage("faasaf.config.yaml");
  const defaultConfig = lodash.merge(parse(rawConfig.toString()), userConfig);

  writeToRepository(".faasafrc", stringify(runConfig));
  writeToRepository("faasaf.config.yaml", stringify(defaultConfig));

  if (inventoryPrompt.confirmation) {
    writeToRepository("hosts/main.yaml", stringify(inventoryPrompt.inventory));
  }

  Object.keys(ansibleInventory.all.hosts).forEach((host) => {
    const provider = host.substring(0, host.indexOf("_host")).replace("_", "-");

    shell.mkdir("-p", `${repositoryPath}/config`);

    fs.copyFileAsync(
      `${packagePath}/config/${provider}.yaml`,
      `${repositoryPath}/config/${provider}.yaml`
    );
  });

  ["remote.yaml", "common.yaml"].forEach((f) =>
    fs.copyFileAsync(
      `${packagePath}/config/${f}`,
      `${repositoryPath}/config/${f}`
    )
  );
};

export const command = {
  name: "init",
  description: "Initialize repository and generate needed files.",
  action,
  options,
  arguments: commandArgs,
};
