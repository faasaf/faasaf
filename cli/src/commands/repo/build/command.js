import chalk from "chalk";
import { logger } from "../../../logger/index.js";
import { program } from "../../../program.js";
import { fetchPipelines, runPipelines } from "./build.js";
import { globals } from "../../../command.js";

const commandArgs = [];

const options = [
  {
    flags: "--target <target>",
    help: "provider preset to use.",
    default: "",
  },
  {
    flags: "--pipeline <pipeline>",
    help: "pipeline to execute.",
  },
  {
    flags: "--no-auto",
    help: "turn off autodiscovery and use packages in config.",
  },
];

const action = async (options) => {
  const { config } = globals;
  const pipelines = await fetchPipelines(config, options);
  let errors = [];
  const results = await runPipelines(config, options, pipelines).then(
    (pipelineResult) => {
      Object.entries(pipelineResult).forEach(([key, tasks]) => {
        const tasksWitherrors = tasks.filter((r) => r.code > 0);
        errors = errors.concat(tasksWitherrors);
        if (tasksWitherrors.length) {
          logger.error(
            `Pipeline "${key}" finished with "${tasksWitherrors.length}" error(s).`,
            tasksWitherrors
          );
        }
      });
      return pipelineResult;
    }
  );

  logResults(results);

  if (errors.length) {
    const msg = `Pipelines finished with "${errors.length}" error(s).`;
    return program.error(chalk.red.bold(msg));
  }

  logger.info(`Pipelines finished successfully.`);
};

export const command = {
  name: "build",
  description: "run build pipeline.",
  options,
  action: action,
  arguments: commandArgs,
};

const logResults = (pipelineResults) => {
  Object.entries(pipelineResults).forEach(async ([pipeline, tasks]) => {
    if (!tasks.length) {
      logger.info(`Pipeline ${pipeline} did not have any tasks.`);
      return;
    }

    const tasksTableData = tasks.map((result) => ({
      name: result.task.name,
      command: result.task.cmd,
      result:
        result.code != 0
          ? chalk.red(`error ${result.code}`)
          : chalk.green("done"),
    }));

    logger.log("table", {
      title: chalk.bold.bgWhite.black(`Results for ${pipeline}:`),
      options: {
        leftPad: 1,
        columns: [
          { field: "name", name: "Name" },
          { field: "command", name: "Command" },
          { field: "result", name: "Result" },
        ],
      },
      data: tasksTableData,
    });
  });
};
