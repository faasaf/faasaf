import { globby } from "globby";
import lodash from "lodash";
import path, { basename } from "path";
import { parse } from "yaml";
import { readFile, repositoryPath } from "../../../utils/fs.js";
import runner from "./runner.js";
import { pipeline } from "./pipeline.js";
import { logger } from "../../../logger/index.js";
import { program } from "commander";

const findPipelinesAuto = (path = "") => {
  const pipelineGlob = `${path}/**/pipeline.yaml`;
  return globby(pipelineGlob, {
    onlyFiles: true,
  });
};

const findPackagePipelines = (packages, path = "") =>
  packages.map((p) => findPipelinesAuto(`${path}/${p}`));

export const fetchPipelines = async (config, options) => {
  const pipelines = await findPipelines(options, config);
  logger.debug("Found pipelines", pipelines);
  const sorted = sortPipelines(pipelines);
  logger.debug("Sorted pipelines", sorted);
  return sorted;
};

export const runPipelines = async (config, options, pipelines) => {
  const results = {};
  logger.log("spinner:start", `⏳`, `loading pipelines...`);
  for (const pipelineConfigPath of pipelines) {
    const projectName = path.basename(path.dirname(pipelineConfigPath));
    logger.log("spinner", `⏳`, `loading pipeline ${projectName}...`);
    let { pipelineConfig, runnerVars } = await processPipelineConfig(
      pipelineConfigPath,
      config
    );

    logger.log("spinner", `⏳`, `running pipeline ${projectName}...`);

    results[pipelineConfigPath] = await pipeline
      .run(
        { ...pipelineConfig, filter: options.target },
        taskHandler(runnerVars)
      )
      .then((tasks) => tasks.filter((t) => !!t));

    logger.log("spinner", `⌛`, `done running pipeline ${projectName}...`);
  }
  logger.log("spinner:stop", `⌛`, `done running pipelines`);

  return results;
};

const taskHandler = (runnerVars) => (task) => runner(task, runnerVars);

const processPipelineConfig = async (pipeline, config) => {
  const projectDir = path.dirname(pipeline);
  const runnerVars = {
    version: config.repository.version,
    image: `${config.repository.registry}/${basename(projectDir)}`,
    provider: config.faasaf_provider,
    config,
    projectDir,
    packageJson: await readFile(`${projectDir}/package.json`)
      .then((p) => JSON.parse(p.toString()))
      .catch((e) => {
        logger.error(`Error reading pipeline config ${pipeline}`, e);
        program.error(`Error reading pipeline config ${pipeline}`)
      }),
  };

  const pipelineConfig = await readFile(pipeline).then((document) =>
    expandVariables(document.toString(), runnerVars)
  );

  return { runnerVars, pipelineConfig };
};

const expandVariables = (document, source) => {
  const variableTest = /[^\\]\$\{([^}]+)\}/;
  let matches = null;
  let lastMatches = null;

  while ((matches = variableTest.exec(document))) {
    if (matches === lastMatches) {
      break;
    }

    lastMatches = matches;
    const path = matches[1];
    document = document.replace(`\${${matches[1]}}`, lodash.get(source, path));
  }

  return parse(document);
};

const sortPipelines = (pipelines) => {
  const lookUpObj = {};

  return pipelines.sort((left, right) => {
    const relativeLeft = path.relative(repositoryPath, left);
    const relativeRight = path.relative(repositoryPath, right);
    if (!lookUpObj[relativeLeft]) {
      const countOfBacksLeft = (relativeLeft.match(/\.\.\/|\//g) || []).length;
      lookUpObj[relativeLeft] = { countOfBacks: countOfBacksLeft };
    }
    if (!lookUpObj[relativeRight]) {
      const countOfBacksRight = (
        relativeRight.match(/(?:\.\.\/)|(?:\/)/g) || []
      ).length;
      lookUpObj[relativeRight] = { countOfBacks: countOfBacksRight };
    }
    return (
      lookUpObj[relativeLeft].countOfBacks -
      lookUpObj[relativeRight].countOfBacks
    );
  });
};

async function findPipelines(options, config) {
  if (options.pipeline) {
    return [options.pipeline];
  }
  const pipelines = options.auto
    ? await findPipelinesAuto(repositoryPath)
    : await findPackagePipelines(config.packages, repositoryPath);
  return pipelines;
}
