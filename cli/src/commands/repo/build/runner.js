import Docker from "dockerode";
import path from "path";
import execSh from "exec-sh";
import { logger } from "../../../logger/index.js";
import chalk from "chalk";

export default (task, runnerVars) => {
  const projectName = path.basename(runnerVars.projectDir);
  logger.log("spinner", `${projectName} -> ${task.name}: `, `Loading task...`);

  if (task.when) {
    logger.log(
      "spinner",
      chalk.black.bgCyan(`${projectName} -> ${task.name}: `),
      chalk.cyan(`skipped`)
    );
    const matches = /([^==|!=]+)(!=|==)(.*)/.exec(task.when);
    if (!compare(matches[1].trim(), matches[2].trim(), matches[3].trim())) {
      return;
    }
  }

  logger.log("spinner", `${projectName} -> ${task.name}: `, "running");

  if (task.image) {
    task.workingDir = process.cwd();
    return runIndocker(
      {
        ...task,
        workingDir: `/opt/repository/${path.relative(
          task.workingDir,
          runnerVars.projectDir
        )}`,
        volumes: {
          "/opt/repository": {},
        },
        binds: [`${task.workingDir}:/opt/repository`],
      },
      runnerVars
    ).then(taskResultCallback(projectName));
  }
  task.workingDir = path.resolve(runnerVars.projectDir);
  return runInShell(task, runnerVars).then(taskResultCallback(projectName));
};

const taskResultCallback = (projectName) => (result) => {
  projectName = projectName ? `${projectName} -> ` : "";
  logger.log(
    "spinner",
    chalk.bgGreen.black(`${projectName}${result.task.name}: `),
    "done"
  );
  return result;
};

const compare = (left, operator, right) =>
  ({
    "==": (left, right) => left === right,
    "!=": (left, right) => left !== right,
  }[operator](left, right));

export const runInShell = async (task) => {
  return execSh
    .promise(task.cmd, { cwd: task.workingDir })
    .then(() => {
      return {
        task,
        code: 0,
      };
    })
    .catch((error) => {
      return {
        task,
        code: error.code,
        error,
      };
    });
};

export const runIndocker = async (task) => {
  const docker = new Docker();

  logger.debug(task);

  if (!task.image) {
    throw {
      message: "Cannot run task without image.",
      task,
    };
  }

  logger.info(`Pulling docker image ${task.image}.`);
  await runInShell({
    cmd: `docker pull ${task.image}`,
    workspace: process.cwd(),
  }).then(() => logger.info(`Done pulling image.`));

  return docker
    .run(task.image, ["/bin/sh", "-c", task.cmd], process.stdout, {
      WorkingDir: task.workingDir,
      Volumes: task.volumes,
      Env: Object.entries(process.env).map(([k, v]) => `${k}=${v}`),
      Hostconfig: {
        Binds: task.binds,
      },
    })
    .then(([output, container]) => {
      container.remove();
      return [output, container];
    })
    .then(([output, container]) => {
      return {
        task,
        code: output.StatusCode,
        error: output.Error,
        details: [[output, container]],
      };
    });
};
