export const pipeline = {
  run: async ({ stages, pipeline, filter }, taskHandler) => {
    const taskRegister = await buildRegister(
      stages,
      pipeline,
      taskHandler,
      filter ?? ""
    );

    return Promise.all(Object.values(taskRegister));
  },
};

/**
 * Gets input stages and pipeines and builds a task registry filled with
 * promises to fullfill the taskHandler.
 * 
 * Input examples: 
 * 
 * stages:
 *   - build
 *   - test
 *   - publish
 * 
 * pipeline:
 *   build:
 *    - name: build
 *      cmd: echo "build"
 *   test:
 *    - name: test
 *      cmd: echo "test"
 *   publish:
 *    - name: publish
 *      cmd: echo "publish"
 *      depends:
 *        - build:build
 * 
 * const taskHandler = (task, runnerVars) => {
 *   console.log(task, runnerVars);
 * }
 * 
 * @param {string[]} stages
 * @param {object} pipeline
 * @param {Promise} taskHandler
 * @returns Record<string, Promise<any>>
 * 
 */
const buildRegister = async (stages, pipeline, taskHandler, filter = "") => {
  const taskRegister = {};
  const [sfilter, tfilter] = filter.split(":");
  let lastStageName = "";

  for (const stageName of stages.filter(
    (s) => s in pipeline && (!sfilter || s === sfilter)
  )) {
    const tasks = pipeline[stageName].filter(
      (t) => !tfilter || tfilter === t.name
    );

    if (!taskRegister[stageName]) {
      taskRegister[stageName] = [];
    }

    tasks.forEach(async (task) => {
      const taskName = `${stageName}:${task.name}`;
      let handler = taskHandler;
      if (task.depends) {
        handler = wrapHandler(
          taskHandler,
          task.depends.map((dep) => taskRegister[dep])
        );
      }

      if (lastStageName && !task.depends) {
        handler = wrapHandler(taskHandler, taskRegister[lastStageName]);
      }

      taskRegister[taskName] = handler(task);
      taskRegister[stageName].push(taskRegister[taskName]);
    });

    taskRegister[stageName] = (async () => {
      await Promise.all(taskRegister[stageName]);
      return null;
    })();

    lastStageName = stageName;
  }

  return taskRegister;
};

const wrapHandler = (taskHandler, waitfor) => async (task) => {
  await Promise.all([waitfor]);
  return taskHandler(task);
};
