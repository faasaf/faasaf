import { runIndocker, runInShell } from "../runner.js";
import { it, describe } from "mocha";
import should from "should";

describe("runner", () => {
  it("runs in docker", async () => {
    const testTask = (exitCode) => ({
      image: "alpine",
      cmd: `exit ${exitCode}`,
      workingDir: "/",
      volumes: {},
      binds: [],
    });
    const max = 255;
    const min = 0;
    const exitCode = Math.round(Math.random() * (max - min) + min);
    const task = testTask(exitCode);
    const result = await runIndocker(task, {});

    should(result.code).be.equal(exitCode);
  }),
    it("runs in shell", async () => {
      const testTask = (exitCode) => ({
        cmd: [`exit ${exitCode}`],
        workingDir: process.cwd(),
      });

      const max = 255;
      const min = 0;
      const exitCode = Math.round(Math.random() * (max - min) + min);
      const task = testTask(exitCode);
      const result = await runInShell(task, {});
      should(result.code).be.equal(exitCode);
    });
});
