import { pipeline } from "../pipeline.js";
import should from "should";
import { describe, it } from "mocha";

const pipelineConfig = {
  stages: ["A", "B", "C"],
  pipeline: {
    A: [{ name: "A", cmd: "A" }],
    B: [{ name: "B", cmd: "B" }],
    C: [{ name: "C", cmd: "C" }],
  },
};

describe("pipeline", () => {
  it("runs stages in sequence.", async () => {
    const results = [];

    const taskHandler = async (task) => {
      results.push(task.name);
      return task;
    };

    const pipelineResults = (
      await pipeline.run(pipelineConfig, taskHandler)
    ).filter((t) => !!t);

    should(results.length).be.equal(3);
    should(results[0]).equal(pipelineConfig.stages[0]);
    should(results[1]).equal(pipelineConfig.stages[1]);
    should(results[2]).equal(pipelineConfig.stages[2]);
    should(pipelineResults[0].name).equal(pipelineConfig.stages[0]);
    should(pipelineResults[1].name).equal(pipelineConfig.stages[1]);
    should(pipelineResults[2].name).equal(pipelineConfig.stages[2]);
  });
  it("filters by stage", async () => {
    const results = [];

    const taskHandler = async (task) => {
      results.push(task.name);
    };

    await pipeline.run({ ...pipelineConfig, filter: "A" }, taskHandler);

    should(results.length).be.equal(1);
    should(results[0]).equal(pipelineConfig.stages[0]);
  });

  it("filters by stage and task", async () => {
    const pipelineConfig = {
      stages: ["A", "B", "C"],
      pipeline: {
        A: [
          { name: "A", cmd: "A" },
          { name: "D", cmd: "D" },
        ],
        B: [{ name: "B", cmd: "B" }],
        C: [{ name: "C", cmd: "C" }],
      },
    };
    const results = [];

    const taskHandler = async (task) => {
      results.push(task.name);
    };

    await pipeline.run({ ...pipelineConfig, filter: "A:D" }, taskHandler);

    should(results.length).be.equal(1);
    should(results[0]).equal("D");
  });
});
