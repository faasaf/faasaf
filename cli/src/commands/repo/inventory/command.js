import { stringify } from "yaml";
import { createInventory, writeInventory } from "./inventory.js";
import { promptInventory } from "./prompts.js";

const commandArgs = [];

const options = [
  {
    flags: "-ivp --inventory-providers <inventory-providers>",
    help: "comma seperated inventory providers",
  },
];

const action = async (options) => {
  const inventory = {
    all: {
      hosts: {},
      children: {},
    },
  };
  if (options.inventoryProviders) {
    return writeInventory(createInventory(options.inventoryProviders));
  }

  const inventoryPrompt = await promptInventory(inventory);
  if (!inventoryPrompt.confirmation) {
    return;
  }

  return writeInventory(stringify(inventoryPrompt.inventory));
};

export const command = {
  name: "inventory",
  description: "Configure Ansible inventory.",
  action,
  arguments: commandArgs,
  options,
};
