import { stringify } from "yaml";
import { writeToRepository } from "../../../utils/fs.js";

export const writeInventory = (inventory) =>
  writeToRepository("hosts/main.yaml", stringify(inventory));

export const createInventory = (input) => {
  const providers = input.split(",");
  const localProviders = [];

  providers.forEach((p) => {
    if (["docker-desktop", "k0s"].includes(p)) {
      localProviders.push(p);
      if (!providers.includes("local")) {
        providers.unshift("local");
      }
    }
  });

  const inventory = localProviders.reduce(
    (carry, current) =>
      formatLocalProvider(formatProvider(carry)(providers))(current),
    { all: { hosts: {}, children: {} } }
  );

  return inventory;
};

export const formatLocalProvider = (inventory) => (p) => {
  const provider = p.replace("-", "_");
  inventory = applyHost(provider, inventory);
  inventory = applyChild(provider, inventory);

  inventory.all.children.local.children[provider] = {};

  return inventory;
};

export const formatProvider = (inventory) => (providers) => {
  if (providers.includes("local")) {
    providers.splice(providers.indexOf("local"), 1);

    inventory = applyHost("local", inventory);
    inventory = setChild(
      "local",
      {
        children: {},
      },
      inventory
    );
  }

  providers.forEach((p) => {
    const provider = p.replace("-", "_");
    inventory = applyHost(provider, inventory);
    inventory = applyChild(provider, inventory);
  });

  return inventory;
};

const applyHost = (provider, inventory) => {
  inventory.all.hosts[`${provider}_host`] = {
    ansible_connection: "local",
    ansible_python_interpreter: "/usr/bin/python3.9",
  };

  return inventory;
};

const applyChild = (provider, inventory) =>
  setChild(
    provider,
    {
      hosts: {
        [`${provider}_host`]: {},
      },
    },
    inventory
  );

const setChild = (provider, child, inventory) => {
  inventory.all.children[provider] = child;
  return inventory;
};
