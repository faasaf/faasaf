import prompts from "prompts";
import { stringify } from "yaml";
import { formatProvider, formatLocalProvider } from "./inventory.js";

export const inventoryPromptItems = (inventory) => [
  {
    type: "multiselect",
    name: "inventory",
    message: "Choose k8s providers to use.",
    format: formatProvider(inventory),
    choices: [
      { title: "Local", value: "local" },
      { title: "Linode", value: "linode" },
      { title: "Azure", value: "azure" },
    ],
  },
  {
    type: (prev) => !!prev.all.children.local && "select",
    name: "inventory",
    message: "Please choose your local k8s provider.",
    format: formatLocalProvider(inventory),
    choices: [
      { title: "Docker Desktop", value: "docker-desktop" },
      { title: "k0s", value: "k0s" },
    ],
  },
  {
    type: "confirm",
    name: "confirmation",
    initial: true,
    message: "Generate inventory?",
    onRender() {
      this.msg = `Generate inventory?
${stringify(inventory)}`;
    },
  },
];

export const promptInventory = async (inventory) =>
  await prompts(inventoryPromptItems(inventory));
