import { command as build } from "./build/index.js";
import { command as init } from "./init/index.js";
import { command as inventory } from "./inventory/index.js";

export default [init, inventory, build];
