import { command as config } from "./config/index.js";
import create from "./create/create.js";
import login from "./faas/login.js";
import newFunction from "./faas/new.js";
import up from "./faas/up.js";
import { command as play } from "./play/index.js";
import version from "./version.js";
import repo from "./repo/index.js";

export default {
  config,
  create,
  faas: [login, newFunction, up],
  play,
  repo,
  version,
};
