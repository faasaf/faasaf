import { readFromPackage } from "../utils/fs.js";
import { globals } from "../command.js";

export default {
  name: "version",
  description: "Shows version informaiton.",
  action: async () => {
    const packageJson = await readFromPackage("package.json").then((p) =>
      JSON.parse(p.toString())
    );
    console.log("CLI Version", packageJson.version);
    console.log("FaaS AF Version", globals?.config?.version);
    console.log("Repository Version", globals?.config?.repository?.version);
  },
};
