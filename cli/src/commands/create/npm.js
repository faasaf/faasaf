import child_process from "node:child_process";

export default (args, target) => {
  try {
    child_process.execFileSync("npm", args, {
      stdio: "inherit",
      cwd: target,
    });
  } catch (e) {
    console.log(e);
  }
};
