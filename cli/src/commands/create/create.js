import { globals } from "../../command.js";
import createVue from "./createVue.js";

const createArguments = [
  {
    name: "type",
    help: "type of project to create",
  },
  {
    name: "target",
    help: "target directory (eg. workspace/apps/myproject)",
  },
];

export default {
  name: "create",
  description: "create project in given directory.",
  action: async (type, target) => {
    if (Object.prototype.hasOwnProperty.call(createMap, type)) {
      createMap[type](globals.config, target);
    }
  },
  arguments: createArguments,
};

const createMap = {
  vue: createVue,
};
