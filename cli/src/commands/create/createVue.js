import lodash from "lodash";
import fs from "node:fs";
import { basename, dirname } from "node:path";
import shell from "shelljs";
import { parse, stringify } from "yaml";
import { globals } from "../../command.js";
import {
  copyTemplate,
  packagePath,
  readFromPackage,
  readFromRepository,
  repositoryPath,
  writeToRepository,
} from "../../utils/fs.js";
import npm from "./npm.js";
import yarn from "./yarn.js";

export default async (target) => {
  shell.mkdir("-p", `${repositoryPath}/${dirname(target)}`);

  const packageJson = await yarnInit(target);

  const vars = {
    name: packageJson.name,
    nameVsafe: packageJson.name.replace("-", "_"),
    target,
    main_domain: globals.config.ansible.main_domain,
  };

  copyTemplate(`${repositoryPath}/${target}`, "vue", vars);
  copyTemplate(repositoryPath, "playbooks", vars);

  let rawConfig = {
    ansible: {},
  };

  if (fs.existsSync(`${packagePath}/faasaf.config.yaml`)) {
    rawConfig = await readFromPackage("faasaf.config.yaml").then((s) =>
      parse(s.toString())
    );
  }

  if (fs.existsSync(`${repositoryPath}/faasaf.config.yaml`)) {
    rawConfig = lodash.merge(
      rawConfig,
      await readFromRepository("faasaf.config.yaml").then((s) =>
        parse(s.toString())
      )
    );
  }

  rawConfig.ansible.function_namespace = "${repository.namespace}-fn";
  rawConfig.ansible.deploy = {
    openfaas: {
      namespace: "${ansible.function_namespace}",
    },
    openfaas_graphql_operator: {
      args: 'namespace="${ansible.function_namespace}"',
      branch: "snowmercury",
      name: "openfaas-graphql-gateway",
      namespace: "graphql-gateway-system",
      deployment_name: "ogg-${ansible.function_namespace}",
      version: "snowmercury",
    },
    [`${vars.nameVsafe}`]: {
      delete_namespace: false,
      domain: "${ansible.main_domain}",
      path: `${target}/kubernetes/resources`,
      hostpath: "${repository.hostpath}",
      name: vars.name,
      namespace: "${repository.namespace}",
      version: "${repository.version}",
    },
  };

  writeToRepository("faasaf.config.yaml", stringify(rawConfig));
};

export const yarnInit = async (target) => {
  npm(
    ["init", "vue@3", basename(target)],
    `${repositoryPath}/${dirname(target)}`
  );
  yarn(["install"], `${repositoryPath}/${target}`);

  const packageJson = JSON.parse(
    await readFromRepository(`${target}/package.json`).then((buffer) =>
      buffer.toString()
    )
  );
  return packageJson;
};
