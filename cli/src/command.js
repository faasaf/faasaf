import commands from "./commands/index.js";
import configYaml from "config-yaml";
import fs from "fs";
import { loadConfig } from "./config/index.js";
import { logger } from "./logger/index.js";
import { readFromPackage, repositoryPath } from "./utils/fs.js";
import os from "os";
import { parse } from "yaml";
import ip from "ip";

export const globals = {
  config: {},
};

export const globalOptions = [
  {
    flags: "-p, --provider <provider>",
    help: "provider preset to use.",
  },
  {
    flags: "--version <version>",
    help: "sets repository version.",
  },
];

export const initProgram = (program) => {
  addCommands(program);
  addGlobalOptions(program);

  program.hook("preAction", async (thisCommand) => {
    var { provider, version } = getOptions(thisCommand);
    globals.config = await loadConfiguration(provider, version);
  });
};

const addOption = (program) => (option) => {
  program.option(option.flags, option.help, option.default);
};

const addCommand = (program, name, command) => {
  const program_command = program.command(name);
  program_command.description = () => command.description;
  program_command.action((...args) => command.action(...args));

  command.arguments?.forEach((arg) => {
    program_command.argument(
      `[${arg.name}]`,
      arg.help,
      arg.default ?? undefined
    );
  });

  command.options?.forEach(addOption(program_command));
};

const addGlobalOptions = (program) => {
  globalOptions.forEach(addOption(program));
  globalOptions.forEach((option) => {
    program.commands.forEach((c) => addOption(c)(option));
  });
};

const addCommands = (program) => {
  Object.entries(commands).forEach(([name, items]) => {
    if (!items.length && items.name) {
      addCommand(program, items.name, items);
      return;
    }

    items.forEach((item) => {
      addCommand(program, `${name}:${item.name}`, item);
    });
  });
};

const loadConfiguration = async (provider, version) => {
  if (!provider) {
    provider = "local";
  }

  logger.debug(`Loading config for provider "${provider}"`);
  const systemDefaultConfig = await readFromPackage("system.yaml").then((s) =>
    parse(s.toString())
  );
  return loadConfig(provider, {
    ...systemDefaultConfig,
    repository: {
      hostpath: repositoryPath,
      hostname: os.hostname(),
      hostip: ip.address(),
      version,
    },
    provider_context: provider,
  });
};

function getOptions(thisCommand) {
  let provider = thisCommand.getOptionValue("provider");
  const version = thisCommand.getOptionValue("version");
  if (!provider && fs.existsSync(`${repositoryPath}/.faasafrc.local`)) {
    const runConfig = configYaml(`${repositoryPath}/.faasafrc.local`);
    provider = runConfig.provider_context;
  }
  return { provider, version };
}
