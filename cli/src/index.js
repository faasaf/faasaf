import * as dotenv from "dotenv";
import { initProgram } from "./command.js";
import {program} from "./program.js"


export default async () => {
  dotenv.config();
  initProgram(program);
  program.parse();
};
