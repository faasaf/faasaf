import lodash from "lodash";

export const expandTemplate = (document, container) => {
  const variableTest = /\$\{([^}]+)\}/;
  let matches = null;
  let lastMatches = null;

  while ((matches = variableTest.exec(document))) {
    if (matches === lastMatches) {
      break;
    }

    lastMatches = matches;

    const path = matches[1];
    document = document.replace(
      `\${${matches[1]}}`,
      lodash.get(container, path)
    );

  }
  return document;
};
