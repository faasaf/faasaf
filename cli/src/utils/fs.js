import path, { dirname } from "path";
import { fileURLToPath } from "url";
import Promise from "bluebird";
import fsRaw from "fs";
import shell from "shelljs";

const fs = Promise.promisifyAll(fsRaw);

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const packagePath = path.resolve(`${__dirname}/../../`);
export const repositoryPath = process.cwd();

export const deleteFromRepository = (path) =>
  fs.rmAsync(`${repositoryPath}/${path}`);

export const writeToRepository = (path, contents) =>
  fs
    .mkdirAsync(dirname(`${repositoryPath}/${path}`), { recursive: true })
    .then(() => fs.writeFileAsync(`${repositoryPath}/${path}`, contents));

export const readFromRepository = (path) =>
  fs.readFileAsync(`${repositoryPath}/${path}`);

export const readFromPackage = (path) =>
  fs.readFileAsync(`${packagePath}/${path}`);

export const readFile = (path) => fs.readFileAsync(path);

export const copyTemplate = (target, layer, vars) => {
  if (fs.existsSync(`${packagePath}/.tmp`)) {
    shell.rm("-r", `${packagePath}/.tmp`);
  }
  console.log(`Copying ${layer} template files`);
  shell.cp("-r", `${packagePath}/templates/${layer}`, `${packagePath}/.tmp`);

  Object.entries(vars).forEach(([key, value]) => {
    const variableRegex = new RegExp(`\\$\\{${key}\\}`, "g");
    const replacementString = `${value}`;

    shell
      .find(`${packagePath}/.tmp`)
      .filter((n) => !fs.lstatSync(n).isDirectory())
      .forEach((file) => {
        fs.writeFileSync(
          file,
          fs
            .readFileSync(file)
            .toString()
            .replace(variableRegex, replacementString)
        );
      });
  });
  shell.cp("-r", `${packagePath}/.tmp/.*`, `${target}/.`);
  shell.cp("-r", `${packagePath}/.tmp/*`, `${target}/.`);
  shell.rm("-r", `${packagePath}/.tmp`);
};
