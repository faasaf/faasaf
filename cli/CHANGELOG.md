# @faasaf/faasaf-cli

## 0.9.11

### Patch Changes

- Fixed repo:init.
- Fixed missing remote.yaml config

## 0.9.9

### Patch Changes

- b84455a: Fixes #29. Cleaned up factory configs.
- 1580759: Fixes #28 empty .faasafrc file

## 0.9.8

### Patch Changes

- 5d5e4b6: Fixed missing provider_context in base config.
