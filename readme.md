# FaaS Application Framework (FaaSAF)

A toolset to develop, deploy and host FaaS frontend applications in any Kubernetes cluster.

Contributions welcome, simply open an issue and start a discussion or directly open a merge request.

Powered by `Docker`, `Ansible`, `kubernetes`, `Docker Desktop`, `kustomize`, `helm`, `k0s`, `bitnami`.
