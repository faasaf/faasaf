# syntax=docker/dockerfile:1
ARG PROVIDER=all

# Base
FROM docker:20.10.16-dind-alpine3.15 AS base

ADD ansible.cfg /opt/provision/ansible.cfg
WORKDIR /opt/provision

RUN apk add \
        --update \
        build-base \
        gcc \
        libc-dev \
        linux-headers  \
        python3-dev \
        libffi-dev \
        openssl-dev \
        bash \
        curl \
        make \
        ansible-core \
        git \
        rsync \
        py3-pip

RUN pip install kubernetes jmespath openshift jsonpatch

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
RUN /bin/bash ./get_helm.sh && rm ./get_helm.sh

RUN curl -sSL https://cli.openfaas.com | sh
RUN cd /usr/local/bin && curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
RUN cd /usr/local/bin && curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
        && chmod +x ./kubectl

RUN mkdir -p /tmp/kubeseal
RUN cd /tmp/kubeseal && wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.18.2/kubeseal-0.18.2-linux-amd64.tar.gz
RUN cd /tmp/kubeseal && tar -xzf kubeseal-0.18.2-linux-amd64.tar.gz && chmod +x ./kubeseal && mv ./kubeseal /usr/local/bin/kubeseal
RUN rm -rf /tmp/kubeseal

RUN ansible-galaxy \
        collection install \
        ansible.posix \
        community.general \
        community.kubernetes \
        kubernetes.core

# Azure 
FROM base AS azure

RUN pip install azure-cli==2.11.1
RUN ansible-galaxy collection install azure.azcollection:==1.8.0 \
        && pip install -r /opt/provision/.ansible/collections/ansible_collections/azure/azcollection/requirements-azure.txt

#  Linode
FROM base AS linode
RUN ansible-galaxy collection install linode.cloud
RUN pip install -r /opt/provision/.ansible/collections/ansible_collections/linode/cloud/requirements.txt
RUN pip install linode-cli

# Local
FROM base AS local

# All
FROM base AS all
COPY --from=local / /
COPY --from=linode / /
COPY --from=azure / /

# Output
FROM $PROVIDER

RUN python3 -m pip cache purge
RUN mkdir -p /etc/.kube && rm -rf /var/cache/apk/*

ADD . /opt/provision
WORKDIR /opt/provision


CMD ["/bin/bash"]
