---
- name: Ensure namespace is present.
  when: state == 'present'
  k8s:
    kind: Namespace
    kubeconfig: "{{ kubeconfig_file }}"
    context: "{{ cluster_name }}-{{ location }}"
    name: "{{ cert_manager.name  }}"
    state: "{{ state }}"
    wait: "{{ state == 'present' }}"

- name: Ensure jetstack helm repository is present.
  kubernetes.core.helm_repository:
    state: "present"
    name: jetstack
    repo_url: https://charts.jetstack.io
  register: _cert_manager_repository

- name: Ensure helm chart is {{ state }}.
  kubernetes.core.helm:
    chart_ref: jetstack/cert-manager
    kubeconfig: "{{ kubeconfig_file }}"
    context: "{{ cluster_name }}-{{ location }}"
    name: "{{ cert_manager.name }}"
    namespace: "{{ cert_manager.name }}"
    release_values: "{{ _cert_manager_release_values }}"
    state: "{{ state }}"
    update_repo_cache: "{{ state == 'present' }}"
    wait: true
  ignore_errors: "{{ state == 'absent' }}"

- name: Ensure jetstack helm repository is absent.
  when: state == 'absent'
  kubernetes.core.helm_repository:
    state: "absent"
    name: jetstack
    repo_url: https://charts.jetstack.io
  register: _cert_manager_repository

- name: Ensure namespace is absent.
  when: state == 'absent'
  k8s:
    kind: Namespace
    kubeconfig: "{{ kubeconfig_file }}"
    context: "{{ cluster_name }}-{{ location }}"
    name: "{{ cert_manager.name  }}"
    state: "{{ state }}"
    wait: "{{ state == 'present' }}"

- name: Ensure ClusterIssuer is {{ state }}.
  when: state == "present"
  k8s:
    kind: ClusterIssuer
    kubeconfig: "{{ kubeconfig_file }}"
    context: "{{ cluster_name }}-{{ location }}" 
    state: "{{ state }}"
    definition:
      apiVersion: cert-manager.io/v1
      kind: ClusterIssuer
      metadata:
        name: letsencrypt
      spec:
        acme:
          server: https://acme-v02.api.letsencrypt.org/directory
          email: "{{ cert_manager.email }}"
          privateKeySecretRef:
            name: letsencrypt
          solvers:
            - http01:
                ingress:
                  class: nginx
