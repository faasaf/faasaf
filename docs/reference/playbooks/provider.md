# Provider

The `provider` playbook provides connectivity to and provisioning of a kubernetes cluster provider.

This is an integral playbook as it provides the user with the needed kubectl configurations to communicate with a kubernetes cluster.

Further more any other playbook depends on identifying the targeted provider and its values in order to function properly.

The providers include

- `local`: A generalization for local k8s clusters, currently supported:
  - `k0s`: Integration with `k0s` typical systems.
  - `docker-desktop`: Integration with Docker Desktop typical system.
- `remote`: A generalization for remote k8s clusters, currently supported:
  - `linode`: Integration with Linode LKE
  - `azure`: Integration with Azure AKS

## Usage

```bash
faasaf-cli play provider [--tags=provider[.k8s|.kubeconfig]]
```

- `provider.k8s` Run tasks to provision a k8s cluster.
- `provider.kubeconfig` Run tasks to fetch kubeconfig for cluster.

The provider can either be set with the `provider_context` configuration, like so

```bash
echo "provider_context: k0s" > .faasafrc.local
```

Or with the `-p, --provider <provider>` argument of `faasaf-cli`:

```
faasaf-cli -p k0s play provider
```

## Configuration

Below is a minimal configuration to setup connectivity with an existing cluster.

```yaml
ansible:
  provider:
    locations:
      - Default
    k8s:
      kubeconfig: true
```

|            key | default | description                       |
| -------------: | :------ | :-------------------------------- |
|      locations | unset   | List of locations of the cluster. |
| k8s.kubeconfig | unset   | Lookup `locations` as context in kubeconfig. |

`provider.locations[0]` lists the `context` name of the kubeconfig to target and `k8s.kubeconfig = true` tells the playbook that the information is to be found in an existing kubeconfig file. By default `~/.kube/config` is used and can be overridden with the `--kubeconfig <kubeconfig>` argument.

`faasaf-cli` then creates a new `kubeconfig` file for each provider and location and sums them up in one file named after the `ansible.cluster_name` config.

## Linode LKE Integration

You will need to provide an API key in a `.env` file in order for `faasaf-cli` and `ansible` to communicate with Linode LKE.

```
LINODE_API_TOKEN=********************
LINODE_CLI_TOKEN=********************
```

```yaml
ansible:
  provider:
    locations:
      - eu-central
    # Configure provider specific node pools.
    node_pools:
      - type: g6-standard-2
        count: 3
        autoscaler:
          enabled: true
          min: 1
          max: 5
```

```bash
faasaf-cli -p linode play provider
```

This will setup an autoscaled LKE cluster for use with `faasaf-cli` in each listed location. In this case the locations equal linode regions.

Please see the [linode.cloud.lke_cluster](https://github.com/linode/ansible_linode/blob/main/docs/modules/lke_cluster.md) documentation for details on the parameters for `node_pools`

## Azure AKS Integration

You will need to create a service principal for your Azure subscription and provider `faasaf-cli` with the required environment variables inside `.env`

```
AZURE_SUBSCRIPTION_ID=***************
AZURE_CLIENT_ID==***************
AZURE_SECRET=***************
AZURE_TENANT=***************
```

```yaml
ansible:
  provider:
    locations:
      - germanywestcentral
    k8s:
      node_pools:
        - name: default
          count: 1
          enable_auto_scaling: yes
          max_count: 5
          min_count: 1
          mode: System
          type: VirtualMachineScaleSets
          vm_size: Standard_D2_v2
```

```bash
faasaf-cli -p azure play provider
```

This will setup an autoscaled AKS cluster for use with `faasaf-cli` in each listed location. In this case the locations equal azure regions.

Please see the [azure.azcollection.azure_rm_aks module](https://docs.ansible.com/ansible/latest/collections/azure/azcollection/azure_rm_aks_module.html#id3) documentation for details on the parameters for `node_pools`.
