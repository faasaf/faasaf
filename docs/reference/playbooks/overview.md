# Playbooks

In order to spin the needed application infrastructure for running
FaaS based software, `faasaf-cli` uses [Ansible Playbooks](https://github.com/ansible/ansible) under the hood.

It is recommended to have [basic knowledge of how playbooks, roles, tasks and Ansible configuration](https://docs.ansible.com/ansible/latest/index.html) work in order to write your own.

## Usage

```bash
➜  faasaf-cli help play
Usage: faasaf-cli play [options] [playbook]

Runs an ansible playbook.

Arguments:
  playbook                   Runs ansible playbook. (default: "*")

Options:
  -t --tags <tags>           comma separated tags to apply in playbook runner. (default: "all")
  -s --state <state>         set state. (present|absent) (default: "present")
  --skip-tags <skipTags>     comma separated tags to skip in playbook runner.
  --kubeconfig <kubeconfig>  path to your local kubeconfig folder. (default: "~/.kube")
  -v --verbose               verbose output.
  -p, --provider <provider>  provider preset to use.
  --version <version>        sets repository version.
  --ip <ip>                  sets host's IP address. (default: "192.168.102.109")
  -h, --help                 display help for command
```

The `play` command runs a series of Ansible playbooks in order to provision a k8s cluster to accommodate FaaS based applications (or any application that can be deployed on a Kubernetes cluster).

Upon execution the command passes a set of configuration values into a prebuilt docker container which runs `ansible-playbook` underneath it all.

While Ansible's use can be as flexible as can be, `faasaf-cli` comes with some opinions about your configuration and how playbooks are written.

All configuration found under the key `ansible` is passed onto `ansible-playbook` as a `json` file and is saved as `.variables.json` in the working directory of `faasaf-cli` (usually the root of your repository).

## Built-in playbooks

`faasaf` comes with a set of built-in playbooks which solve common problems in different domains.

- [Provider](./provider.md)
  Provides tasks to provision a cluster and fetch needed kubeconfig files.
- Cluster
  Provides tasks to provision:
  - cluster facing ingress
  - Certificate management using cert-manager
  - Docker registry
  - DNS to point to cluster ingress
  - Service operators for databases, GraphQL services etc, or any provider specific Kubernetes Operators
- FaaS
  Provides tasks to provision an OpenFaaS instance to be used by the cluster.
  Furthermore it deploys another Operator which is mediator between GraphQL service operator and OpenFaaS functions (see [Add FaaS to your project](../../getting-started/faas.md))

Running `faasaf-cli play` without any arguments will run the above 3 playbooks in succession. It would be the same as calling

```bash
# Run `provider` playbook
faasaf-cli play provider

# Run `cluster` playbook
faasaf-cli play cluster

# Run `faas` playbook.
faasaf-cli play faas
```

## Custom Playbooks

## Using Helm

## Using Kustomize

## Using Databases
