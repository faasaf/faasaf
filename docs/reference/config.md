# Configuration Reference

## Configuration syntax

Configuration for faasaf-cli is generally written in `YAML` format with a little bit of extra on top.
Inspired by Ansible's configuration templates, it is possible to use variables within the configuration file.

Take for example the default configuration for any repository created with `faasaf-cli`

Configuration can reference itself by using the variable substitution enclosed in `${}`

```yaml{3-4}
repository:
  name: faasaf-cli
  namespace: ${repository.name}
  registry: registry.${ansible.main_domain}
  version: 0.0.0
```

A configuration value may also contain a javascript function, for example in order to set `repository.version` to have either the value of the environment variable `VERSION`, or if `VERSION` does not exist or has a falsy value, `repository.version` is set to `"0.0.0"`

```yaml{5}
repository:
  name: faasaf-cli
  namespace: ${repository.name}
  registry: registry.${ansible.main_domain}
  version: () => this.config.env.VERSION ?? "0.0.0"
```

Further more other `yaml` files can be included in order to build a flexible and DRY configuration for your cluster.

Take for example the first few lines of the `config/local.yaml` file. 
It includes the `faasaf.config.yaml` found in the root of the repository and overrides/adds values to the configuration.


```yaml
imports:
  - resource: ../faasaf.config.yaml

ansible:
  main_domain: ${ansible.cluster_name}.localdev
```

This loading pattern allows `faasaf-cli` to be configured in layers (similar to kustomize).

If for example one wishes to further more differentiate between `local` and `k0s`, another configuration `config/k0s.yaml` can be created. 


## File Structure

A repository has the following configuration structure:

```
|- config               # Provider specific configurations
|  |- local.yaml        # Configuration for local. Includes `../faasaf.config.yaml`
|  |- k0s.yaml          # Configuration for k0s. Includes `./local.yaml`
|  |- remote.yaml       # Configuration for remote cluster. Includes `../faasaf.config.yaml`
|  |- linode.yaml       # Configuration for linode cluster. Includes `./remote.yaml`
|  |- azure.yaml       # Configuration for linode cluster. Includes `./remote.yaml`
|
|- hosts
|  |- main.yaml         # Ansible hosts configuration
|
|- .env                 # Environment Variables
|- .faasafrc            # Runtime configuration
|- .faasafrc.local      # Local configuration overrides
|- faasaf.config.yaml   # Basic repository configuration
```

While here are many configuration files listed, their contents are put together by `faasaf-cli` into one
configuration object.

There are basically 3 parts of the confiugration.

1. Runtime configuration.
2. `repository`: Repository meta information.
3. `ansible`: Ansible variables.

The files in the `config` folder are primarily named after their `provider` counterpart. By default the `provider` is set to `local` and [can be changed on runtime](playbooks/provider.md).



## 1. Runtime configuration (.faasafrc)

<<< @/../cli/.faasafrc{yaml}

|              key | default         | description                                                              |
| ---------------: | :-------------- | :----------------------------------------------------------------------- |
|          version | `blackchicken`  | Version of `faasaf-cli` and its components                               |
|       playboooks | `[playbooks/*]` | Location of Ansible playbooks                                            |
| provider_context | `local`         | Provider context to use (`k0s`, `docker-desktop`, `linode`, `azure` ...) |

## 2. Repository meta information

<<< @/../cli/faasaf.config.yaml#repository-meta

|       key | default                           | description                                 |
| --------: | :-------------------------------- | :------------------------------------------ |
|      name | `faasaf-cli`                      | Name of the repository                      |
| namespace | `${repository.name}`              | Repository namespace (used for deployments) |
|  registry | `registry.${ansible.main_domain}` | Docker registry used to this repository     |
|   version | `1.0.0`                           | Version of the repository                   |

## 3. Ansible Variables

This section is where most of the configuration will happen as cluster operations are executed under the hood by Ansible playbooks.

<<< @/../cli/faasaf.config.yaml#ansible-general

|          key | default                       | description                        |
| -----------: | :---------------------------- | :--------------------------------- |
| cluster_name | `faasaf`                      | Name of the cluster to use/deploy. |
|  main_domain | `${ansible.cluster_name}`     | Main domain to use in deployments. |
| main_contact | `info@${ansible.main_domain}` | Main contact email.                |

