# Setup K0s

This section will cover how to install and configure [k0s](https://k0sproject.io/) to follow along the [Initialize a project from scratch.](./initialize-project.md)

## 1. Install k0s

```
curl -sSLf https://get.k0s.sh | sudo sh
sudo mkdir -p /etc/k0s
sudo k0s config create | sudo tee /etc/k0s/k0s.yaml
```

## 2. Configure k0s

```bash
sudo vim /etc/k0s/k0s.yaml
```

Change the following lines:

```yaml
extensions:
  storage:
    type: openebs_local_storage
```

## 3. Run k0s

```bash
sudo k0s install controller --single
sudo k0s start
sudo k0s kubectl get nodes -w

sudo k0s kubeconfig admin > ~/.kube/config
kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```
