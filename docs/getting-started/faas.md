# Add FaaS to your project

`faasaf-cli` provides an interface to easily write and deploy OpenFaaS functions.
For this `faasaf-cli` utilizes the OpenFaaS command line utility `faas-cli`.

## Requirements

- `faas-cli` https://docs.openfaas.com/cli/install/
  ```bash
  curl -sSL https://cli.openfaas.com | sudo -E sh
  ```

## Add a new function to the project

By default the OpenFaaS instance is secured by a Basic Auth username and password. `faasaf-cli` wraps the `faas-cli` in order to facilitate automated login and processes function configurations like any other `faasaf-cli` configuration file.

```bash
# Login to the cluster OpenFaaS service.
faasaf-cli faas:login

# Create a new function based on the `node17` template.
faasaf-cli faas:new sample-function workspace/functions --lang node17
```

## Adjust function.

By default a `graphql-gateway` operator is deployed in the cluster, which is interfaced by `openfaas-graphql-gateway` in order assign functions to automatically deployed graphql servers. This makes it possible to define a GraphQL Schema for each function and have the function assign itself to public facing servers.

Adjust the contents of the `workspace/functions/sample-function.yml` in order to add GraphQL capabilities to the function.

<<< @/sample_files/sample-function.yaml{10-11,12-28}

In order for the `openfaas-graphql-gateway` to recognize the `sample-function` the label `gql_function: true` must be set. This will let `openfaas-graphql-gateway` to read out the `backend`, `gateways` and `schema` annotations.

|      key | description                                                                   |
| -------: | :---------------------------------------------------------------------------- |
|  backend | Specifies the OpenFaas backend to use.                                        |
| gateways | List of public facing gateways (creates Kubernetes Services with port `3000`) |
|   schema | GraphQL Schema used for this function.                                        |

---

Also adjust the `content-type` in `workspace/functions/sample-function/handler.js`.

<<< @/sample_files/sample-function.js

## Add Ingress Path for graphql-gateway.

Last but not least, you will need to create an ingress for the newly create GraphQL service `public-api-graphql-gateway`.

Add a new path to `kubernetes/resources/base/ingress.yaml`

```yaml
spec:
  rules:
    - host: ${DOMAIN}
      http:
        paths:
          - path: /(graphql/?.*)
            pathType: Prefix
            backend:
              service:
                name: public-api-graphql-gateway
                port:
                  number: 4000
```
