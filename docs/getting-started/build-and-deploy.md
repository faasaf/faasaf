# Build and deploy.

Now we build and deploy the whole project once more.

```bash
# Run pipeline to build and publish vue3 application.
faasaf-cli repo:build

# Deploy vue3 application.
faasaf-cli play deploy

# Build and deploy OpenFaaS functions.
faasaf-cli faas:up
```

View https://www.myproject.localdev/ in your browser
or browse your functions GraphQL in https://www.myproject.localdev/graphql.
