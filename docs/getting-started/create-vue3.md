# Create Vue3 application.

When creating the vue appication, make sure you create it with
`Typescript` support.

```bash
# Create vue3 application and deployment playbook.
faasaf-cli create vue workspace/apps/vue-project

# Run pipeline to build and publish vue3 application to the docker registry.
faasaf-cli repo:build #-p k0s or -p docker-desktop

# Run playbook `deploy/index.yaml` to deploy the vue3 application.
faasaf-cli play deploy #-p k0s or -p docker-desktop

# Wait for vue3 application to be deployed.
kubectl -n myproject rollout status deployment/vue-project
```

Now you can view your instance in https://www.myproject.localdev
