# Initialize a project from scratch.

## 0. Requirements.

- Access to a local Kubernetes instance and kubectl.

  FaaSaf has only been tested with Docker Desktop for Mac and k0s on Ubuntu 20.04, however it should not be a problem to get it to work with a few configuration changes.

  To follow this guide it would be easiest to start off with a completely fresh cluster.

  If you have no cluster setup you can easily create one with Docker Desktop on MacOs or follow the [Setup k0s](./setup-k0s.md) guide.

- OpenFaaS cli client https://github.com/openfaas/faas-cli#get-started-install-the-cli

- `npm` or `yarn` to install `faasaf-cli`

  ```bash
  npm config set @faasaf:registry https://gitlab.com/api/v4/projects/28481095/packages/npm/
  npm install -g @faasaf/faasaf-cli
  faasaf-cli help
  ```

## 1. Initialize project.

In order to initialize your repository simply run the following command.

```bash
mkdir myproject && cd "$_"
faasaf-cli repo:init
```

This command will guide you through your first steps of configuring your repository.

::: details 1.2. Configuration (Optional).

### 1.1. Configuration (Optional).

The `repo:init` command creates a few configuration files.
The `config` folder holds provider configurations enabled for this repository.
An [Ansible inventory](https://docs.ansible.com/ansible/latest/inventory_guide/index.html) is generated inside the `hosts` folder.

#### 1.1.1 Ansible inventory.

To configure clusters, Ansible's inventory system is used.
For each provider to be used a host must be entered to the `hosts/main.yaml`

This can be managed through the cli with the `repo:inventory` command.

```bash
faasaf-cli repo:inventory
```

#### 1.1.2 Ansible variables.

Each node inside `ansible` represents a component that can be enabled or disabled.
For configuration of each component please refer to the respective component documentation.

Commenting any of the nodes inside `ansible` will disable that feature,
for example if you uncomment the `cluster` node, you will lose the `cert-manager` and `service-operator` capabilities, respectively you can omit either of them to pick and choose.

:::

## 2. Provision your cluster.

In order to provision your cluster the `faasaf-cli play` command can be used.
The `faasaf-cli play` command runs one or a pre-defined list of playbooks to be executed given a the current provider's configuration.

For more information please refer to the [Configuration Reference](../reference/config.md).

```bash
faasaf-cli play -p k0s
# or
faasaf-cli play -p docker-desktop
```

::: info
To provision your cluster you will need to run the `play` command with the `-p` flag in order to set the `provider`.

In order to omit the `-p` flag you can enter the `provider_context` in `.faasafrc.local`.

```bash
echo "provider_context: k0s" >> .faasafrc.local
#or
echo "provider_context: docker-desktop" >> .faasafrc.local
```

:::

::: warning
Make sure that port `30053` is open to serve the local DNS server.
:::

## 3. Post-installation steps.

The default setup for local providers includes deployments for a docker registry and a DNS service in order to route domains to the cluster. Since local providers issue self-signed SSL certificates, the root CA of the cluster's `cert-manager` needs to be installed on the system. Without the installation of this Root CA, services `k0s` or Docker will not be able to pull images from the docker registry.

### 3.1. Trust root CA.

#### Ubuntu 22.04

```bash
# Install system pre-requisites.
sudo apt install -y ca-certificates

# Fetch the root CA from `cert-manager`.
kubectl -n cert-manager get secrets myproject-ca-secret -o json \
  | jq -r '.data["ca.crt"]' \
  | base64 -d \
  | sudo tee /usr/local/share/ca-certificates/myproject-ca.crt
# Update system ca certificates
sudo update-ca-certificates

# Restart docker and k0s
sudo systemctl restart docker.service
sudo systemctl restart k0scontroller.service
```

In order for the certificate to be trusted by Firefox and Chrome you will have to import the CA Root Certificate manually into each one of them.

#### MacOS

To add the CA certificate to in MacOS first we need to download the certificate:

```bash
# Fetch the root CA from `cert-manager`.
kubectl -n cert-manager get secrets root-ca-secret -o json \
  | jq -r '.data["ca.crt"]' \
  | base64 -d \
  > ~/Downloads/myproject-ca.crt
```

Then double click the file in the finder and follow the wizard.

### 3.2. Configure DNS.

#### Ubuntu 22.04

```bash
# Create a new resolve.conf.d file and add localhost:30053 as an additional DNS server.
sudo mkdir -p /usr/local/lib/systemd/resolved.conf.d/
cat <<EOF | sudo tee /usr/local/lib/systemd/resolved.conf.d/myproject-dns.conf
nameserver 127.0.0.1:30053

[Resolve]
DNS=127.0.0.1:30053
EOF

# Restart resolved
sudo systemctl restart systemd-resolved
```

#### MacOS

Simply add `127.0.0.1` to the list of DNS servers in your network settings.
