export default {
  title: "FaaSaf",
  description: "FaaS Application Framework",
  base: `${process.env.BASE || "/"}`,
  themeConfig: {
    sidebar: [
      {
        text: "Getting Started",
        link: "/getting-started/index",
        items: [
          {
            text: "Initialize a project from scratch",
            link: "/getting-started/initialize-project",
          },
          {
            text: "Create Vue 3 Application",
            link: "/getting-started/create-vue3",
          },
          {
            text: "Add FaaS to your project",
            link: "/getting-started/faas",
          },
          {
            text: "Build and deploy",
            link: "/getting-started/build-and-deploy",
          },
        ],
      },
      {
        text: "Reference",
        items: [
          {
            text: "Configuration",
            link: "/reference/config",
          },
          {
            text: "Playbooks",
            items: [
              {
                text: "Overview",
                link: "/reference/playbooks/overview",
              },
              {
                text: "Provider",
                link: "/reference/playbooks/provider",
              },
            ],
          },
        ],
      },
    ],
  },
};
