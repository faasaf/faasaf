# FaaS Application Framework

## About

FaaSaf aims to provide a toolset to create, develop, deploy and maintain,
complex modern Applications using FaaS or Microservices architectures.

The framework aims to take care of the following tasks:

- Build and configure a K8s cluster on different providers
- Provision clusters with common services including but not limited to
  - Service Operators: On demand services for applications to use
    - Databases
    - Search engines (elasticsearch, meilisearch...)
    - Identity services
    - Auth services
    - OpenFaaS services
  - Certificate management
  - cluster ingresses
  - OpenFaaS access
- Provide tools for managing development environments.
- Provide tools for building and deploying applications.
- Provide tools for building and deploying functions.
